package com.authit.myapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class AccountDetailsActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);
        Bundle extras = getIntent().getExtras();

        String scanResult;
        String scanType;

        if (extras != null) {
            scanResult = extras.getString("scan_result");
            scanType = extras.getString("scan_type");
            EditText accountName = (EditText)findViewById(R.id.account_details_input_account_name);
            accountName.setHint(scanResult);
            EditText accountSecretKey = (EditText)findViewById(R.id.account_details_input_secret_key);
            accountSecretKey.setHint(scanType);
        } else {
            //todo add spinner options, totp, hotp etc.

        }
    }

    public void buttonOk(View v){
        // todo save user settings, navigate to the main activity
    }

    public void buttonCancel(View v){
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_account_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
