package com.authit.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import android.util.Log;
import android.widget.Toast;

public class ScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        //todo parse auth string example: otpauth://totp/Jagex:wqamember?secret=35UMS462&issuer=Jagex
        mScannerView.stopCamera();
        if(rawResult.getText().toLowerCase().contains("otpauth")){
            Log.e("otpauth found",rawResult.getText());
            Intent accountDetailsActivity = new Intent(this, AccountDetailsActivity.class);
            accountDetailsActivity.putExtra("scan_result",rawResult.getText());
            accountDetailsActivity.putExtra("scan_type", rawResult.getBarcodeFormat().toString());
            this.startActivity(accountDetailsActivity);
            this.finish();
        } else {
            Log.e("non valid found",rawResult.getText());
            Toast.makeText(this, "Invalid scan please scan a valid otpauth barcode", Toast.LENGTH_LONG).show();
            mScannerView.startCamera();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
