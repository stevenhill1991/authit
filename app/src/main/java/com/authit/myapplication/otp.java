package com.authit.myapplication;

import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.codec.binary.Base32;

import javax.crypto.KeyGenerator;
import java.security.NoSuchAlgorithmException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Steve on 09/06/2015.
 */
public class otp {

        public static String encodeSecretKey(String key){
        return new String(new Base32().encode(key.getBytes()));
    }
    
    public static String decodeSecretKey(String key){
        return new String(new Base32().decode(key.getBytes()));
    }

    public static String totp(String secretKey){
        byte[] key = secretKey.getBytes();
        long timestep = 30;
        long counter = unixTimeNow()/timestep;
        System.out.println(counter);

        return hotp(key,counter);
    }

    public static String hotp(byte[] key, long counter){
        int tokenLength = 6;
        try {
            byte[] hash = getHmacSHA1(String.valueOf(counter), key);

            int offset = hash[hash.length - 1] & 0xf;
            System.out.println("offset" + offset);
            int binary =
                    ((hash[offset] & 0x7f) << 24) |
                            ((hash[offset + 1] & 0xff) << 16) |
                            ((hash[offset + 2] & 0xff) << 8) |
                            (hash[offset + 3] & 0xff);

            System.out.println("binary full int code before split " + binary);
            String totp = Integer.toString(binary%1000000);
            while(totp.length() < 6){
               totp = "0" + totp;
            }
            return totp;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "false";
    }
    
    public static byte[] getHmacSHA1(String data, byte[] key) throws InvalidKeyException, NoSuchAlgorithmException {

        SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        return mac.doFinal(data.getBytes());
    }
    
    public static long unixTimeNow(){
        return System.currentTimeMillis()/1000;
    }
}
